import React from 'react';

import BillTable from './components/BillTable';

function EasyCharge() {
  return (
    <div>
      <BillTable />
    </div>
  );
}

export default EasyCharge;

var express = require('express');
var path = require('path');
var router = express.Router();

/* GET test page. */
router.get('/', function(req, res, next) {
    res.send("test home page");
});

router.get('/test2', function (req, res, next) {
    res.send("test2");
});

// 映射静态资源路径 
router.use(express.static(path.join(__dirname, '../public')));

router.get('/test', function (req, res, next) {
    res.sendFile(path.join(__dirname, '../public/test.html'));
});

module.exports = router;

// HelloQuad.js
// Vertex shader 顶点着色器
var VSHADER_SOURCE = 
  "attribute vec4 a_Position;\n" + 
  "attribute float a_PointSize;\n" + 
  "void main() {\n" + 
  " gl_Position = a_Position;\n" + 
  // " gl_PointSize = a_PointSize;\n" + 
  "}\n";

// Fragment shader 片段着色器
// 注意：只能指定float类型的attribute变量，但是uniform变量可以是任意类型的，所以使用前需要指定uniform变量的类型
var FSHADER_SOURCE = 
  "precision mediump float;\n" + 
  "uniform vec4 u_FragColor;\n" + 
  "void main() {\n" + 
  " gl_FragColor = u_FragColor;\n" + 
  "}\n";

function main() {
  var canvas = document.getElementById("webgl");

  var gl = getWebGLContext(canvas);
  if (!gl) {
    console.log("Failed to get the rendering context for WebGL");
    return;
  }

  if (!initShaders(gl, VSHADER_SOURCE, FSHADER_SOURCE)) {
    console.log("Fialed to initialize shaders.");
    return;
  }

  var u_FragColor = gl.getUniformLocation(gl.program, "u_FragColor");
  if (u_FragColor < 0) {
    console.log("Failed to get the storage location of u_FragColor");
    return;
  }
  gl.uniform4f(u_FragColor, 1.0, 0.0, 0.0, 1.0);

  // 设置顶点信息
  var n = initVertexBuffers(gl);
  if (n < 0) {
    console.log("Failed to set the position of the vertices.");
    return;
  }

  gl.clearColor(0.0, 0.0, 0.0, 1.0);

  gl.clear(gl.COLOR_BUFFER_BIT);

  /**
   * gl.drawArrays(mode, first, count);
   * mode:
   *  gl.POINTS 点
   *  gl.LINES  绘制一系列单独的线段 (v0, v1)、(v2, v3)... 如果是奇数个点，最后一个点被忽略
   *  gl.LINE_STRIP 绘制一系列连接的线段 (v0, v1)、(v1, v2)...
   *  gl.LINE_LOOP 绘制连接的线段，最后一个点会与第一个点相连
   *  gl.TRIANGLES 绘制一系列单独的三角形
   *  gl.TRIANGLE_STRIP 绘制一系列条状带三角形(多个三角形组成的梯形) (v0, v1, v2)、(v2, v1, v3)、(v2, v3, v4) 后面的三角形会共享前一个三角形的一条边，需要注意绘制点的顺序 
   *  gl.TRIANGLE_FAN 一系列三角形组成的类似扇形的图形 (v0, v1, v2)、(v0, v2, v3)、(v0, v3, v4)...
   */
  gl.drawArrays(gl.TRIANGLE_STRIP, 0, n);
}

function initVertexBuffers(gl) {
  // 需要注意顶点的顺序，第二个点和第三个点应该是斜边 这两个点组成的线段是共用的
  var vertices = new Float32Array(
    [
      -0.5, 0.5,
      -0.5, -0.5, 
      0.5, 0.5,
      0.5, -0.5
    ]);
  var n = 4;

  // 创建缓冲区对象
  var vertexBuffer = gl.createBuffer();
  // gl.deleteBuffer(vertexBuffer);
  if (!vertexBuffer) {
    console.log("Failed to create the buffer object.");
    return -1;
  }

  // 绑定缓冲区对象
  gl.bindBuffer(gl.ARRAY_BUFFER, vertexBuffer);
  // 缓冲区对象写入数据
  gl.bufferData(gl.ARRAY_BUFFER, vertices, gl.STATIC_DRAW);

  var a_Position = gl.getAttribLocation(gl.program, "a_Position");
  if (a_Position < 0) {
    console.log("Failed to get the storage location of a_Position");
    return -1;
  }

  // 将缓冲区对象分配给a_Position对象
  /**
   * gl.vertexAttribPointer(location, size, type, normalized, stride, offset);
   *  location: attribute location
   *  size: 每次赋值给attribute的元素个数，vec4，不够4个用0或1(vec4[3])补充
   *  type: 元素类型
   *  normalized: 是否归一化
   *  stride: 相邻两个顶点间的字节数(默认是0)
   *  offset: 开始位置的偏移(字节数)
   */
  gl.vertexAttribPointer(a_Position, 2, gl.FLOAT, false, 0, 0);
  // 连接a_Position变量与分配给它的缓冲区对象
  gl.enableVertexAttribArray(a_Position);
  // gl.disableVertexAttribArray(a_Position);

  return n;
}
// MatrixTriangle.js
// Vertex shader 顶点着色器
var VSHADER_SOURCE = 
  "attribute vec4 a_Position;\n" + 
  "uniform mat4 u_translationMatrix;\n" + 
  "uniform mat4 u_ScaleMatrix;\n" + 
  "void main() {\n" + 
  " gl_Position = u_translationMatrix * u_ScaleMatrix * a_Position;\n" + 
  "}\n";

// Fragment shader 片段着色器
// 注意：只能指定float类型的attribute变量，但是uniform变量可以是任意类型的，所以使用前需要指定uniform变量的类型
var FSHADER_SOURCE = 
  "precision mediump float;\n" + 
  "uniform vec4 u_FragColor;\n" + 
  "void main() {\n" + 
  " gl_FragColor = u_FragColor;\n" + 
  "}\n";

var ANGLE = 90;

function main() {
  var canvas = document.getElementById("webgl");

  var gl = getWebGLContext(canvas);
  if (!gl) {
    console.log("Failed to get the rendering context for WebGL");
    return;
  }

  if (!initShaders(gl, VSHADER_SOURCE, FSHADER_SOURCE)) {
    console.log("Fialed to initialize shaders.");
    return;
  }

  setUniform4f(gl, "u_FragColor", 1.0, 0.0, 0.0, 1.0);

  // 设置顶点信息
  var n = initVertexBuffers(gl);
  if (n < 0) {
    console.log("Failed to set the position of the vertices.");
    return;
  }

  // 转为弧度制
  var radian = Math.PI * ANGLE / 180.0;
  var cosB = Math.cos(radian);
  var sinB = Math.sin(radian);
  var Tx = 0.5, Ty = 0.5, Tz = 0.0;
  var Sx = 0.5, Sy = 0.5, Sz = 0.5;

  /**
   * 变换矩阵
   *  Sx,Sy,Sz控制缩放
   *  cosB,-sinB,sinB控制旋转(这里是绕Z轴旋转)
   *  Tx,Ty,Tz控制平移
   * 
   * cosB*Sx  -sinB     0.0 Tx
   * sinB     cosB*Sy   0.0 Ty
   * 0.0      0.0       Sz  Tz
   * 0.0      0.0       0.0 1
   * 
   * 注意：WebGL的矩阵是列主序
   */
  var matrix = new Float32Array(
    [
      cosB, sinB, 0.0, 0.0,
      -sinB, cosB, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0,
      Tx, Ty, Tz, 1
    ]
  );
  var u_translationMatrix = gl.getUniformLocation(gl.program, "u_translationMatrix");
  if (u_translationMatrix < 0) {
    return;
  }
  // 第二个参数表示是否转置矩阵，WebGL没有实现该操作，必须为false
  gl.uniformMatrix4fv(u_translationMatrix, false, matrix);

  matrix = new Float32Array(
    [
      Sx, 0.0, 0.0, 0.0,
      0.0, Sy, 0.0, 0.0,
      0.0, 0.0, Sz, 0.0,
      0.0, 0.0, 0.0, 1
    ]
  );
  var u_ScaleMatrix = gl.getUniformLocation(gl.program, "u_ScaleMatrix");
  if (u_ScaleMatrix < 0) {
    return;
  }
  gl.uniformMatrix4fv(u_ScaleMatrix, false, matrix);

  gl.clearColor(0.0, 0.0, 0.0, 1.0);

  gl.clear(gl.COLOR_BUFFER_BIT);

  gl.drawArrays(gl.TRIANGLES, 0, n);
}

function initVertexBuffers(gl) {
  var vertices = new Float32Array(
    [
      -0.5, -0.5, 
      0.5, -0.5,
      0, 0.5
    ]);
  var n = 3;

  // 创建缓冲区对象
  var vertexBuffer = gl.createBuffer();
  // gl.deleteBuffer(vertexBuffer);
  if (!vertexBuffer) {
    console.log("Failed to create the buffer object.");
    return -1;
  }

  // 绑定缓冲区对象
  gl.bindBuffer(gl.ARRAY_BUFFER, vertexBuffer);
  // 缓冲区对象写入数据
  gl.bufferData(gl.ARRAY_BUFFER, vertices, gl.STATIC_DRAW);

  var a_Position = gl.getAttribLocation(gl.program, "a_Position");
  if (a_Position < 0) {
    console.log("Failed to get the storage location of a_Position");
    return -1;
  }

  // 将缓冲区对象分配给a_Position对象
  gl.vertexAttribPointer(a_Position, 2, gl.FLOAT, false, 0, 0);
  // 连接a_Position变量与分配给它的缓冲区对象
  gl.enableVertexAttribArray(a_Position);
  // gl.disableVertexAttribArray(a_Position);

  return n;
}
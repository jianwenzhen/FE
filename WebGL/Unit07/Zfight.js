// Zfight.js
// 深度(Z轴)冲突

// Vertex shader 顶点着色器
var VSHADER_SOURCE = 
  "attribute vec4 a_Position;\n" + 
  "attribute vec4 a_Color;\n" + 
  "varying vec4 v_Color;\n" + 
  "void main() {\n" + 
  " gl_Position = a_Position;\n" + 
  " v_Color = a_Color;\n" + 
  "}\n";

// Fragment shader 片段着色器
var FSHADER_SOURCE = 
  "#ifdef GL_ES\n" + 
  "precision mediump float;\n" + 
  "#endif\n" + 
  "varying vec4 v_Color;\n" + 
  "void main() {\n" + 
  " gl_FragColor = v_Color;\n" + 
  "}\n";

var DEBUG = false;

class MatInfo {
  constructor(location, matrix)  {
    this.location = location;
    this.matrix = matrix;
  }
}

var canvas;
function main() {
  canvas = document.getElementById("webgl");

  var gl = getWebGLContext(canvas);
  if (!gl) {
    console.log("Failed to get the rendering context for WebGL");
    return;
  }

  if (!initShaders(gl, VSHADER_SOURCE, FSHADER_SOURCE)) {
    console.log("Fialed to initialize shaders.");
    return;
  }

  // 设置顶点信息
  var n = initVertexBuffers(gl);
  if (n < 0) {
    console.log("Failed to set the position of the vertices.");
    return;
  }

  // 开启深度检测
  // gl.enable(gl.DEPTH_TEST);

  if (!DEBUG) {
    document.getElementById("debug").remove();
  } else {
    var frameEle = document.getElementById("frame");
  }

  gl.clearColor(0.0, 0.0, 0.0, 1.0);

  gl.clear(gl.COLOR_BUFFER_BIT);

  gl.drawArrays(gl.TRIANGLES, 0, n);
  // gl.drawArrays(gl.TRIANGLE_STRIP, 0, n);
}

function initVertexBuffers(gl) {
  var vertices = new Float32Array(
    [
      -0.8, -0.5, -0.5, 0.6, 1.0, 0.8,
      -0.2, -0.5, -0.5, 0.6, 1.0, 0.8,
      -0.6,  0.5, -0.5, 0.6, 1.0, 0.8,

      -0.7, -0.5, -0.5, 1.0, 0.5, 1.0,
      -0.1, -0.5, -0.5, 1.0, 0.5, 1.0,
      -0.6,  0.4, -0.5, 1.0, 0.5, 1.0,
    ]);
  var n = 6;

  // 创建缓冲区对象
  var vertexBuffer = gl.createBuffer();
  // gl.deleteBuffer(vertexBuffer);
  if (!vertexBuffer) {
    console.log("Failed to create the buffer object.");
    return -1;
  }

  // 绑定缓冲区对象
  gl.bindBuffer(gl.ARRAY_BUFFER, vertexBuffer);
  // 缓冲区对象写入数据
  gl.bufferData(gl.ARRAY_BUFFER, vertices, gl.STATIC_DRAW);

  var FSIZE = vertices.BYTES_PER_ELEMENT;
  var a_Position = gl.getAttribLocation(gl.program, "a_Position");
  if (a_Position < 0) {
    console.log("Failed to get the storage location of a_Position");
    return -1;
  }

  // 将缓冲区对象分配给a_Position对象
  gl.vertexAttribPointer(a_Position, 3, gl.FLOAT, false, FSIZE * 6, 0);
  // 连接a_Position变量与分配给它的缓冲区对象
  gl.enableVertexAttribArray(a_Position);
  // gl.disableVertexAttribArray(a_Position);

  var a_Color = gl.getAttribLocation(gl.program, "a_Color");
  gl.vertexAttribPointer(a_Color, 3, gl.FLOAT, false, FSIZE * 6, FSIZE * 3);
  gl.enableVertexAttribArray(a_Color);

  return n;
}

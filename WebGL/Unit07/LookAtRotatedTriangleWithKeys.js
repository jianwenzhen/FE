// LookAtRotatedTriangleWithKeys.js
// Vertex shader 顶点着色器
var VSHADER_SOURCE = 
  "attribute vec4 a_Position;\n" + 
  "attribute vec4 a_Color;\n" + 
  "uniform mat4 u_ViewMatrix;\n" + 
  "uniform mat4 u_RotatedMatrix;\n" + 
  "uniform mat4 u_ViewModelMatrix;\n" + 
  "varying vec4 v_Color;\n" + 
  "void main() {\n" + 
  // " gl_Position = u_ViewMatrix * a_Position;\n" + 
  // " gl_Position = u_ViewMatrix * u_RotatedMatrix * a_Position;\n" + 
  " gl_Position = u_ViewModelMatrix * a_Position;\n" + 
  " v_Color = a_Color;\n" + 
  "}\n";

// Fragment shader 片段着色器
// 注意：只能指定float类型的attribute变量，但是uniform变量可以是任意类型的，所以使用前需要指定uniform变量的类型
var FSHADER_SOURCE = 
  "#ifdef GL_ES\n" + 
  "precision mediump float;\n" + 
  "#endif\n" + 
  "varying vec4 v_Color;\n" + 
  "void main() {\n" + 
  " gl_FragColor = v_Color;\n" + 
  "}\n";

var DEBUG = false;

function main() {
  var canvas = document.getElementById("webgl");

  var gl = getWebGLContext(canvas);
  if (!gl) {
    console.log("Failed to get the rendering context for WebGL");
    return;
  }

  if (!initShaders(gl, VSHADER_SOURCE, FSHADER_SOURCE)) {
    console.log("Fialed to initialize shaders.");
    return;
  }

  gl.clearColor(0.0, 0.0, 0.0, 1.0);

  gl.clear(gl.COLOR_BUFFER_BIT);

  // 设置顶点信息
  var n = initVertexBuffers(gl);
  if (n < 0) {
    console.log("Failed to set the position of the vertices.");
    return;
  }

  initViewMatrix(gl);

  if (!DEBUG) {
    document.getElementById("debug").remove();
  } else {
    var frameEle = document.getElementById("frame");
  }

  gl.drawArrays(gl.TRIANGLES, 0, n);
}

function initVertexBuffers(gl) {
  // 注意图形的顺序，这里还没有计算深度，所以后面的图形即使位于后面，但是因为最后才画出来也会覆盖位于前面(Z轴)的图形
  var vertices = new Float32Array(
    [
      -0.5, -0.5, -0.4, 0.0, 0.0, 1.0,
      0.5,  -0.5, -0.4, 0.0, 0.0, 1.0,
      0,     0.5, -0.4, 0.0, 0.0, 1.0,

      -0.5, 0.5, -0.2, 0.0, 1.0, 0.0,
      0.5,  0.5, -0.2, 0.0, 1.0, 0.0,
      0,    -0.5, -0.2, 0.0, 1.0, 0.0,

      -0.5, -0.5, 0.0, 1.0, 0.0, 0.0,
      0.5,  -0.5, 0.0, 1.0, 0.0, 0.0,
      0,     0.5, 0.0, 1.0, 0.0, 0.0,

      0.0,  0.5,  -0.4,  0.4,  1.0,  0.4, // The back green one
      -0.5, -0.5,  -0.4,  0.4,  1.0,  0.4,
       0.5, -0.5,  -0.4,  1.0,  0.4,  0.4, 
     
       0.5,  0.4,  -0.2,  1.0,  0.4,  0.4, // The middle yellow one
      -0.5,  0.4,  -0.2,  1.0,  1.0,  0.4,
       0.0, -0.6,  -0.2,  1.0,  1.0,  0.4, 
  
       0.0,  0.5,   0.0,  0.4,  0.4,  1.0,  // The front blue one 
      -0.5, -0.5,   0.0,  0.4,  0.4,  1.0,
       0.5, -0.5,   0.0,  1.0,  0.4,  0.4, 
    ]);
  var n = 9;

  // 创建缓冲区对象
  var vertexBuffer = gl.createBuffer();
  // gl.deleteBuffer(vertexBuffer);
  if (!vertexBuffer) {
    console.log("Failed to create the buffer object.");
    return -1;
  }

  // 绑定缓冲区对象
  gl.bindBuffer(gl.ARRAY_BUFFER, vertexBuffer);
  // 缓冲区对象写入数据
  gl.bufferData(gl.ARRAY_BUFFER, vertices, gl.STATIC_DRAW);

  var FSIZE = vertices.BYTES_PER_ELEMENT;
  var a_Position = gl.getAttribLocation(gl.program, "a_Position");
  if (a_Position < 0) {
    console.log("Failed to get the storage location of a_Position");
    return -1;
  }

  // 将缓冲区对象分配给a_Position对象
  gl.vertexAttribPointer(a_Position, 3, gl.FLOAT, false, FSIZE * 6, 0);
  // 连接a_Position变量与分配给它的缓冲区对象
  gl.enableVertexAttribArray(a_Position);
  // gl.disableVertexAttribArray(a_Position);

  var a_Color = gl.getAttribLocation(gl.program, "a_Color");
  gl.vertexAttribPointer(a_Color, 3, gl.FLOAT, false, FSIZE * 6, FSIZE * 3);
  gl.enableVertexAttribArray(a_Color);

  return n;
}

function initViewMatrix(gl) {
  // 视角矩阵
  var viewMatrix = new Matrix4();
  // 视点、目标、上方向
  viewMatrix.setLookAt(0.2, 0.25, 0.25, 0, 0, 0, 0, 1, 0);
  var u_ViewMatrix = gl.getUniformLocation(gl.program, "u_ViewMatrix");
  gl.uniformMatrix4fv(u_ViewMatrix, false, viewMatrix.elements);

  // 旋转矩阵
  var rotatedMatrix = new Matrix4();
  rotatedMatrix.setRotate(10, 0, 0, 1);
  var u_RotatedMatrix = gl.getUniformLocation(gl.program, "u_RotatedMatrix");
  gl.uniformMatrix4fv(u_RotatedMatrix, false, rotatedMatrix.elements);

  // 在JS中运算好在赋值，提高效率
  var viewModelMatrix = new Matrix4();
  viewModelMatrix = viewMatrix.multiply(rotatedMatrix);
  // 另一种简化写法
  //viewModelMatrix.setLookAt(0.2, 0.25, 0.25, 0, 0, 0, 0, 1, 0).rotate(10, 0, 0, 1);
  var u_ViewModelMatrix = gl.getUniformLocation(gl.program, "u_ViewModelMatrix");
  gl.uniformMatrix4fv(u_ViewModelMatrix, false, viewModelMatrix.elements);
}

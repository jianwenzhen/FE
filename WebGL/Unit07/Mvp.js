// Mvp.js

// Vertex shader 顶点着色器
var VSHADER_SOURCE = 
  "attribute vec4 a_Position;\n" + 
  "attribute vec4 a_Color;\n" + 
  "uniform mat4 u_MvpMatrix;\n" + 
  "varying vec4 v_Color;\n" + 
  "void main() {\n" + 
  " gl_Position = u_MvpMatrix * a_Position;\n" + 
  " v_Color = a_Color;\n" + 
  "}\n";

// Fragment shader 片段着色器
var FSHADER_SOURCE = 
  "#ifdef GL_ES\n" + 
  "precision mediump float;\n" + 
  "#endif\n" + 
  "varying vec4 v_Color;\n" + 
  "void main() {\n" + 
  " gl_FragColor = v_Color;\n" + 
  "}\n";

var DEBUG = false;

class MatInfo {
  constructor(location, matrix)  {
    this.location = location;
    this.matrix = matrix;
  }
}

var canvas;
function main() {
  canvas = document.getElementById("webgl");

  var gl = getWebGLContext(canvas);
  if (!gl) {
    console.log("Failed to get the rendering context for WebGL");
    return;
  }

  if (!initShaders(gl, VSHADER_SOURCE, FSHADER_SOURCE)) {
    console.log("Fialed to initialize shaders.");
    return;
  }

  gl.clearColor(0.0, 0.0, 0.0, 1.0);

  gl.clear(gl.COLOR_BUFFER_BIT);

  // 设置顶点信息
  var n = initVertexBuffers(gl);
  if (n < 0) {
    console.log("Failed to set the position of the vertices.");
    return;
  }

  // 开启深度检测
  gl.enable(gl.DEPTH_TEST);

  initMvpMatrix(gl);

  // document.onkeydown = function (event) { keydownHandler(event, gl, n, modelInfo, viewInfo, projInfo); };

  if (!DEBUG) {
    document.getElementById("debug").remove();
  } else {
    var frameEle = document.getElementById("frame");
  }

  gl.drawArrays(gl.TRIANGLES, 0, n);
}

function initVertexBuffers(gl) {
  var vertices = new Float32Array(
    [
      // left
      -0.8, -0.5, -4, 0.4, 0.0, 1.0,
      -0.2, -0.5, -4, 0.4, 0.0, 1.0,
      -0.6,  0.5, -4, 0.4, 0.8, 0.2,

      -0.8, -0.5, -10, 1.0, 0.8, 0.4,
      -0.2, -0.5, -10, 0.4, 0.8, 0.4,
      -0.6,  0.5, -10, 0.4, 0.5, 0.3,

      -0.8, -0.5, -20, 1.0, 0.0, 0.2,
      -0.2, -0.5, -20, 1.0, 0.4, 0.2,
      -0.6,  0.5, -20, 0.0, 0.4, 0.0,

      // right
      0.8, -0.5, -4, 0.4, 0.0, 1.0,
      0.2, -0.5, -4, 0.4, 0.0, 1.0,
      0.6,  0.5, -4, 0.4, 0.8, 0.2,

      0.8, -0.5, -10, 0.0, 1.0, 0.0,
      0.2, -0.5, -10, 0.0, 1.0, 0.0,
      0.6,  0.5, -10, 0.0, 1.0, 0.0,

      0.8, -0.5, -20, 1.0, 0.0, 0.0,
      0.2, -0.5, -20, 1.0, 0.0, 0.0,
      0.6,  0.5, -20, 1.0, 0.0, 0.0,
    ]);
  var n = 18;

  // 创建缓冲区对象
  var vertexBuffer = gl.createBuffer();
  // gl.deleteBuffer(vertexBuffer);
  if (!vertexBuffer) {
    console.log("Failed to create the buffer object.");
    return -1;
  }

  // 绑定缓冲区对象
  gl.bindBuffer(gl.ARRAY_BUFFER, vertexBuffer);
  // 缓冲区对象写入数据
  gl.bufferData(gl.ARRAY_BUFFER, vertices, gl.STATIC_DRAW);

  var FSIZE = vertices.BYTES_PER_ELEMENT;
  var a_Position = gl.getAttribLocation(gl.program, "a_Position");
  if (a_Position < 0) {
    console.log("Failed to get the storage location of a_Position");
    return -1;
  }

  // 将缓冲区对象分配给a_Position对象
  gl.vertexAttribPointer(a_Position, 3, gl.FLOAT, false, FSIZE * 6, 0);
  // 连接a_Position变量与分配给它的缓冲区对象
  gl.enableVertexAttribArray(a_Position);
  // gl.disableVertexAttribArray(a_Position);

  var a_Color = gl.getAttribLocation(gl.program, "a_Color");
  gl.vertexAttribPointer(a_Color, 3, gl.FLOAT, false, FSIZE * 6, FSIZE * 3);
  gl.enableVertexAttribArray(a_Color);

  return n;
}

function initMvpMatrix(gl) {
  // 模型矩阵
  var modelMatrix = new Matrix4();
  modelMatrix.setRotate(0, 0, 0, 1);

  // 视角矩阵
  var viewMatrix = new Matrix4();
  // 视点、目标、上方向
  viewMatrix.setLookAt(0.0, 0.0, 0.0, 0, 0, -1, 0, 1, 0);

  var projMatrix = new Matrix4();
  // fov视角 宽高比 近点 远点
  projMatrix.setPerspective(30, canvas.width/canvas.height, 1.0, 1000.0);

  var mvpMatrix = new Matrix4();
  mvpMatrix.set(projMatrix).multiply(viewMatrix).multiply(modelMatrix);
  var u_MvpMatrix = gl.getUniformLocation(gl.program, "u_MvpMatrix");
  gl.uniformMatrix4fv(u_MvpMatrix, false, mvpMatrix.elements);
}

function initModelMatrix(gl) {
  // 模型矩阵
  var modelMatrix = new Matrix4();
  modelMatrix.setRotate(0, 0, 0, 1);
  var u_ModelMatrix = gl.getUniformLocation(gl.program, "u_ModelMatrix");
  gl.uniformMatrix4fv(u_ModelMatrix, false, modelMatrix.elements);
  return new MatInfo(u_ModelMatrix, modelMatrix);
}

function initViewMatrix(gl) {
  // 视角矩阵
  var viewMatrix = new Matrix4();
  // 视点、目标、上方向
  viewMatrix.setLookAt(0.0, 0.0, 0.0, 0, 0, -1, 0, 1, 0);
  var u_ViewMatrix = gl.getUniformLocation(gl.program, "u_ViewMatrix");
  gl.uniformMatrix4fv(u_ViewMatrix, false, viewMatrix.elements);
  return new MatInfo(u_ViewMatrix, viewMatrix);
}

function initProjectiveMatrix(gl) {
  var projMatrix = new Matrix4();
  // fov视角 宽高比 近点 远点
  projMatrix.setPerspective(30, canvas.width/canvas.height, 1.0, 1000.0);
  var u_ProjMatrix = gl.getUniformLocation(gl.program, "u_ProjMatrix");
  gl.uniformMatrix4fv(u_ProjMatrix, false, projMatrix.elements);
  return new MatInfo(u_ProjMatrix, projMatrix);
}

var g_eyeX = 0.0, g_eyeY = 0.0, g_eyeZ = 0.0;
function keydownHandler(event, gl, n, modelInfo, viewInfo, projInfo) {
  var key = event.keyCode;
  switch (key) {
    // 左
    case 37:
      g_eyeX -= 0.1;
      if (g_eyeX < -1.0) {
        g_eyeX = -1.0;
      }
      break;
    // 上
    case 38:
      g_eyeY += 0.1;
      if (g_eyeY > 1.0) {
        g_eyeY = 1.0;
      }
      break;
    // 右
    case 39:
      g_eyeX += 0.1;
      if (g_eyeX > 1.0) {
        g_eyeX = 1.0;
      }
      break;
    // 下
    case 40:
      g_eyeY -= 0.1;
      if (g_eyeY < -1.0) {
        g_eyeY = -1.0;
      }
      break;
    default:
      return;
  }

  draw(gl, n, modelInfo, viewInfo, projInfo);
}

function draw(gl, n, modelInfo, viewInfo, projInfo) {
  var viewMatrix = viewInfo.matrix;
  viewMatrix.setLookAt(g_eyeX, g_eyeY, g_eyeZ, 0, 0, -1, 0, 1, 0);
  gl.uniformMatrix4fv(viewInfo.location, false, viewMatrix.elements);

  gl.clear(gl.COLOR_BUFFER_BIT);
  // gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
  gl.drawArrays(gl.TRIANGLES, 0, n);
}

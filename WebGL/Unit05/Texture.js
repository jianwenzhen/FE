// Texture.js

var VSHADER_SOURCE = 
  "attribute vec4 a_Position;\n" + 
  "attribute vec2 a_TexCoord;\n" + 
  "varying vec2 v_TexCoord;\n" + 
  "void main() {\n" + 
  " gl_Position = a_Position;\n" + 
  " v_TexCoord = a_TexCoord;\n" + 
  "}\n";

var FSHADER_SOURCE = 
  "precision mediump float;\n" + 
  "uniform sampler2D u_Sampler1;\n" + 
  "uniform sampler2D u_Sampler2;\n" + 
  "varying vec2 v_TexCoord;\n" + 
  "void main() {\n" + 
  " vec4 color0 = texture2D(u_Sampler1, v_TexCoord);\n" + 
  " vec4 color1 = texture2D(u_Sampler2, v_TexCoord);\n" + 
  " gl_FragColor = color0 * color1;\n" +
  "}\n";

var DEBUG = false;
var g_last = Date.now();
var g_frame = 0;
var FRAME_SAMPLE_INTERVAL = 10;

function main() {
  var canvas = document.getElementById("webgl");

  var gl = getWebGLContext(canvas);
  if (!gl) {
    console.log("Failed to get the rendering context for WebGL");
    return;
  }

  if (!initShaders(gl, VSHADER_SOURCE, FSHADER_SOURCE)) {
    console.log("Failed to initialize shaders.");
    return;
  }

  var n = initVertexBuffers(gl);
  if (n < 0) {
    console.log("Failed to set the position of the vertices.");
    return;
  }

  if (!initTextures(gl, n)) {
    console.log("Failed to initialize texture.");
    return;
  }

  

  if (!DEBUG) {
    document.getElementById("debug").remove();
  } else {
    var frameEle = document.getElementById("frame");
  }

  // var i = 0;
  // var tick = function () {
  //   animate(currentAngle);
  //   requestAnimationFrame(tick);

  //   ++i;
  //   if (i === FRAME_SAMPLE_INTERVAL) {
  //     i = 0;
  //     frameEle.innerHTML = Math.floor(g_frame / FRAME_SAMPLE_INTERVAL);
  //     g_frame = 0;
  //   }
  // };

  // tick();
}

function initVertexBuffers(gl) {
  var vertices = new Float32Array(
    [
      -0.5, -0.5, 0.0, 0.0,
      -0.5,  0.5, 0.0, 1.0,
       0.5, -0.5, 1.0, 0.0,
       0.5,  0.5, 1.0, 1.0
    ]);
  var n = 4;

  // 创建缓冲区对象
  var vertexBuffer = gl.createBuffer();
  // gl.deleteBuffer(vertexBuffer);
  if (!vertexBuffer) {
    console.log("Failed to create the buffer object.");
    return -1;
  }

  // 绑定缓冲区对象
  gl.bindBuffer(gl.ARRAY_BUFFER, vertexBuffer);
  // 缓冲区对象写入数据
  gl.bufferData(gl.ARRAY_BUFFER, vertices, gl.STATIC_DRAW);

  var FSIZE = vertices.BYTES_PER_ELEMENT;
  var a_Position = gl.getAttribLocation(gl.program, "a_Position");
  if (a_Position < 0) {
    console.log("Failed to get the storage location of a_Position");
    return -1;
  }

  gl.vertexAttribPointer(a_Position, 2, gl.FLOAT, false, FSIZE * 4, 0);
  gl.enableVertexAttribArray(a_Position);
  // gl.disableVertexAttribArray(a_Position);

  var a_TexCoord = gl.getAttribLocation(gl.program, "a_TexCoord");
  gl.vertexAttribPointer(a_TexCoord, 2, gl.FLOAT, false, FSIZE * 4, FSIZE * 2);
  gl.enableVertexAttribArray(a_TexCoord);

  return n;
}

function initTextures(gl, n) {
  var texture0 = gl.createTexture();
  var texture1 = gl.createTexture();
  var u_Sampler1 = gl.getUniformLocation(gl.program, "u_Sampler1");
  var u_Sampler2 = gl.getUniformLocation(gl.program, "u_Sampler2");

  var image0 = new Image();
  image0.onload = function () { loadTexture(gl, n, texture0, u_Sampler1, image0, 0); };
  image0.src = "http://localhost/WebGL/resources/0.jpg";

  var image1 = new Image();
  image1.onload = function () { loadTexture(gl, n, texture1, u_Sampler2, image1, 1); };
  image1.src = "http://localhost/WebGL/resources/1.png";

  return true;
}

var tex0_active = false, tex1_active = false;                                                                                                      
function loadTexture(gl, n, texture, sampler, image, imageNo) {
  // 对纹理图像进行Y轴反转(纹理图像的Y轴坐标和图片是相反的)
  gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, 1);

  if (imageNo == 0) {
    gl.activeTexture(gl.TEXTURE0);
    tex0_active = true;
  } else {
    gl.activeTexture(gl.TEXTURE1);
    tex1_active = true;
  }
  // 向target绑定纹理对象 纹理有TEXTURE_2D和TEXTURE_CUBE_MAP
  gl.bindTexture(gl.TEXTURE_2D, texture);

  // 配置纹理参数 第二个参数是key，第三个参数是值
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
  // 配置纹理图像
  gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, image);

  // 将纹理传递给着色器
  gl.uniform1i(sampler, imageNo);

  if (tex0_active && tex1_active) {
    gl.clearColor(0.0, 0.0, 0.0, 1.0);
    gl.clear(gl.COLOR_BUFFER_BIT);

    gl.drawArrays(gl.TRIANGLE_STRIP, 0, n);
  }
}

function animate(angle) {
  var now = Date.now();
  var elapsed = now - g_last;
  g_last = now;

  if (DEBUG) {
    g_frame += (1000.0 / elapsed);
  }
}

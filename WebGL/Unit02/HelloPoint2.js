// HelloPoint2.js
// Vertex shader 顶点着色器
var VSHADER_SOURCE = 
  "attribute vec4 a_Position;\n" + 
  "attribute float a_PointSize;\n" + 
  "void main() {\n" + 
  " gl_Position = a_Position;\n" + 
  " gl_PointSize = a_PointSize;\n" + 
  "}\n";

// Fragment shader 片段着色器
var FSHADER_SOURCE = 
  "void main() {\n" + 
  " gl_FragColor = vec4(1.0, 0.0, 0.0, 1.0);\n" + 
  "}\n";

function main() {
  var canvas = document.getElementById("webgl");

  var gl = getWebGLContext(canvas);
  if (!gl) {
    console.log("Failed to get the rendering context for WebGL");
    return;
  }

  if (!initShaders(gl, VSHADER_SOURCE, FSHADER_SOURCE)) {
    console.log("Fialed to initialize shaders.");
    return;
  }

  // 获取变量a_Position的存储位置
  var a_Position = gl.getAttribLocation(gl.program, "a_Position");
  if (a_Position < 0) {
    console.log("Failed to get the storage location of a_Position");
    return;
  }

  // 将定点位置传输给attribute变量
  // gl.vertexAttrib1f(a_Position, 0.5);
  // gl.vertexAttrib2f(a_Position, 0.5, 0.0);
  gl.vertexAttrib3f(a_Position, 1.0, 0.0, 0.0);
  // gl.vertexAttrib4f(a_Position, 0.5, 0.0, 0.0, 1.0);

  // 使用向量的版本
  // var position = new Float32Array([1.0, 0.0, 0.0, 1.0]);
  // gl.vertexAttrib4fv(a_Position, position);

  var a_PointSize = gl.getAttribLocation(gl.program, "a_PointSize");
  if (a_PointSize < 0) {
    console.log("Failed to get the storage location of a_PointSize");
    return;
  }
  gl.vertexAttrib1f(a_PointSize, 5.0);

  gl.clearColor(0.0, 0.0, 0.0, 1.0);

  gl.clear(gl.COLOR_BUFFER_BIT);

  gl.drawArrays(gl.POINTS, 0, 1);
}
// HelloCanvas.js
function main() {
  var canvas = document.getElementById("webgl");

  // 获取WebGL绘图上下文
  var gl = getWebGLContext(canvas);
  if (!gl) {
    console.log("Failed to retrieve the <canvas> element");
    return;
  }

  // 清空指定canvas的颜色
  gl.clearColor(1.0, 0.0, 0.0, 0.1);
  // 清空canvas
  gl.clear(gl.COLOR_BUFFER_BIT);
}
// ColoredPoint.js
// Vertex shader 顶点着色器
var VSHADER_SOURCE = 
  "attribute vec4 a_Position;\n" + 
  "attribute float a_PointSize;\n" + 
  "void main() {\n" + 
  " gl_Position = a_Position;\n" + 
  " gl_PointSize = a_PointSize;\n" + 
  "}\n";

// Fragment shader 片段着色器
// 注意：只能指定float类型的attribute变量，但是uniform变量可以是任意类型的，所以使用前需要指定uniform变量的类型
var FSHADER_SOURCE = 
  "precision mediump float;\n" + 
  "uniform vec4 u_FragColor;\n" + 
  "void main() {\n" + 
  " gl_FragColor = u_FragColor;\n" + 
  "}\n";

function main() {
  var canvas = document.getElementById("webgl");

  var gl = getWebGLContext(canvas);
  if (!gl) {
    console.log("Failed to get the rendering context for WebGL");
    return;
  }

  if (!initShaders(gl, VSHADER_SOURCE, FSHADER_SOURCE)) {
    console.log("Fialed to initialize shaders.");
    return;
  }

  // 获取变量a_Position的存储位置
  var a_Position = gl.getAttribLocation(gl.program, "a_Position");
  if (a_Position < 0) {
    console.log("Failed to get the storage location of a_Position");
    return;
  }

  var a_PointSize = gl.getAttribLocation(gl.program, "a_PointSize");
  if (a_PointSize < 0) {
    console.log("Failed to get the storage location of a_PointSize");
    return;
  }
  gl.vertexAttrib1f(a_PointSize, 5.0);

  var u_FragColor = gl.getUniformLocation(gl.program, "u_FragColor");
  if (u_FragColor < 0) {
    console.log("Failed to get the storage location of u_FragColor");
    return;
  }

  // 注册canvas的鼠标点击事件响应函数
  canvas.onmousedown = function(event) {
    click(event, gl, canvas, a_Position, u_FragColor);
  }

  gl.clearColor(0.0, 0.0, 0.0, 1.0);

  gl.clear(gl.COLOR_BUFFER_BIT);
}

var g_points = [];
var g_colors = [];
function click(event, gl, canvas, a_Position, u_FragColor) {
  var x = event.clientX;
  var y = event.clientY;
  // (rect.left, rect.top)是canvas的原点在浏览器中的坐标
  var rect = event.target.getBoundingClientRect();
  x = ((x - rect.left) - canvas.height/2)/(canvas.height/2);
  y = ((canvas.width/2 - (y - rect.top)))/(canvas.width/2);
  g_points.push([x, y]);
  if (x >= 0.0 && y >= 0.0) {
    g_colors.push([1.0, 0.0, 0.0, 1.0]);
  } else if (x < 0.0 && y < 0.0) {
    g_colors.push([0.0, 1.0, 0.0, 1.0]);
  } else {
    g_colors.push([0.0, 0.0, 1.0, 1.0]);
  }

  // 绘制点之后 颜色缓冲器会被WebGL重置为默认颜色(0.0, 0.0, 0.0, 0.0)
  gl.clear(gl.COLOR_BUFFER_BIT);

  var len = g_points.length;
  for (var i = 0; i < len; ++i) {
    var xy = g_points[i];
    var rgba = g_colors[i];

    gl.vertexAttrib3f(a_Position, xy[0], xy[1], 0.0);
    gl.uniform4fv(u_FragColor, rgba);
    //gl.uniform4f(u_FragColor, rgba[0], rgba[1], rgba[2], 1.0);

    gl.drawArrays(gl.POINTS, 0, 1);
  }
}
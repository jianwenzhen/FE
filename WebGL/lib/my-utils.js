function setUniform1f(gl, name, val) {
  var u_Location = gl.getUniformLocation(gl.program, name);
  if (u_Location < 0) {
    console.log("Failed to get the storage location of " + name);
    return;
  }
  gl.uniform1f(u_Location, val);
}

function setUniform2f(gl, name, v0, v1) {
  var u_Location = gl.getUniformLocation(gl.program, name);
  if (u_Location < 0) {
    console.log("Failed to get the storage location of " + name);
    return;
  }
  gl.uniform2f(u_Location, v0, v1);
}

function setUniform3f(gl, name, v0, v1, v2) {
  var u_Location = gl.getUniformLocation(gl.program, name);
  if (u_Location < 0) {
    console.log("Failed to get the storage location of " + name);
    return;
  }
  gl.uniform3f(u_Location, v0, v1, v2);
}

function setUniform4f(gl, name, v0, v1, v2, v3) {
  var u_Location = gl.getUniformLocation(gl.program, name);
  if (u_Location < 0) {
    console.log("Failed to get the storage location of " + name);
    return;
  }
  gl.uniform4f(u_Location, v0, v1, v2, v3);
}

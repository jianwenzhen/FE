// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
// 引入 element-ui 及其 css样式
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
// 引入Iconfont 图标
import './assets/icon/iconfont.css'
// 引入router
import router from './router'
// 引入axios
import axios from 'axios'
// 引入vuex
import Vuex from 'vuex'

Vue.use(Vuex)
Vue.use(ElementUI, { size: 'small' })
// Vue.use(ElementUI)

// 将axios添加为Vue的原型属性
Vue.prototype.$axios = axios
Vue.config.productionTip = false

// Vuex 创建一个store，注意这里的顺序，要在使用store之前先创建好
// 通过 store.state 来获取状态对象 
// 通过 store.commit 方法触发状态变更
const store = new Vuex.Store({
  state: {
    loggedin: false
  },
  mutations: {
    loginSuccess (state) {
      state.loggedin = true
    },
    logout (state) {
      state.loggedin = false
    }
  }
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  // 注册 store 选项，这可以把 store 的实例注入所有的子组件
  store,
  components: { App },
  template: '<App/>'
})

// 默认跳转到登录页
// router.beforeEach((to, from, next) => {
//   if (to.fullPath !== '/') {
//     next({
//       path: '/'
//     })
//     return
//   }
//   next()
// })

import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Users from '@/components/Users'
import Login from '@/components/Login'
import BackPwd from '@/components/BackPwd'
import Layout from '@/components/Layout'
import Layout2 from '@/components/Layout2'
import Layout3 from '@/components/Layout3'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/backPwd',
      name: 'BackPwd',
      component: BackPwd
    },
    {
      path: '/users',
      name: 'Users',
      component: Users
    },
    {
      path: '/layout',
      name: 'Layout',
      component: Layout
    },
    {
      path: '/layout2',
      name: 'Layout2',
      component: Layout2
    },
    {
      path: '/layout3',
      name: 'Layout3',
      component: Layout3
    },
  ],
  mode: 'history'
})

// 实例化Vue对象
new Vue({
    el: "#vue-app",
    data: {
        name: "SoneWinstone",
        age: 24
    },
    methods: {
        inputName() {
            this.name = this.$refs.name.value
        },
        inputAge() {
            this.age = this.$refs.age.value
        }
    }
});

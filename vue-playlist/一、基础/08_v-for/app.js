// 实例化Vue对象
new Vue({
    el: "#vue-app",
    data: {
        characters: ["Mario", "Luffy", "Yoshi"],
        users: [
            {name: "Henry", age: 30},
            {name: "Winstone", age: 24},
        ]
    },
    methods: {

    },
    computed: {
        
    }
});

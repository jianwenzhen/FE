// 实例化Vue对象
new Vue({
    el: "#vue-app",
    data: {
        a: 0,
        b: 0,
        age: 24
    },
    methods: {
        // addToA() {
        //     console.log("Add To A")
        //     return (this.a + this.age)
        // },
        // addToB() {
        //     console.log("Add To B")
        //     return (this.b + this.age)
        // }
    },
    computed: {
        addToA() {
            console.log("Add To A")
            return (this.a + this.age)
        },
        addToB() {
            console.log("Add To B")
            return (this.b + this.age)
        }
    }
});

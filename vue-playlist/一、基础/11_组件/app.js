Vue.component("greeting", {
    // 一个template只能有一个根标签

    // template:'<p>{{name}}: Hello Component!</p>',
    // ES6语法
    template:
    `
        <p>
            {{name}}: Hello Component!
            <button @click='changeName'>改名</button>
        </p>
    `,
    data: function() {
        return {
            name: "Winstone"
        }
    },
    methods: {
        changeName() {
            this.name = "Another";
        }
    }
})

var one = new Vue({
    el: "#App1"
});

var two = new Vue({
    el: "#App2"
});


var one = new Vue({
    el: "#App1",
    data: {
        title: "App1"
    },
    methods: {
        greet() {
            return "Hello App1"
        }
    },
    computed: {
        
    }
});

var two = new Vue({
    el: "#App2",
    data: {
        title: "App2"
    },
    methods: {
        greet() {
            return "Hello App2"
        },
        changeTitle() {
            one.title = "changed App1"
        }
    },
    computed: {
        
    }
});

two.title = "changed App2"

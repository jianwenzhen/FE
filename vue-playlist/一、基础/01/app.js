// 实例化Vue对象
new Vue({
    el: "#vue-app",
    data: {
        name: "SoneWinstone",
        job: "Java Engineer",
        website: "https://blog.sone.vip",
        websiteTag: "<a href=https://blog.sone.vip>Blog</a>",
    },
    methods: {
        greet: function (word) {
            return 'Hello ' + word + ' ' + this.name + '!';
        }
    }
});

/**
 * el: element 需要获取/控制的元素，一定是html中的根容器元素
 * data: 用于数据的存储
 * methods: 用于存储各种方法
 * data-binding: 给属性绑定对应的值
 */
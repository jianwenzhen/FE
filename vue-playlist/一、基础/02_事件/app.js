// 实例化Vue对象
new Vue({
    el: "#vue-app",
    data: {
        name: "SoneWinstone",
        age: 24,
        x: 0,
        y: 0,
    },
    methods: {
        subtract() {
            this.age--;
        },
        add: function (num) {
            this.age += num;
        },
        updateXY(event) {
            // console.log(event)
            this.x = event.offsetX
            this.y = event.offsetY
        },
        stopMoving(event) {
            // JS阻止冒泡事件
            event.stopPropagation();
        },
        alert() {
            alert("阻止默认事件");
        }
    }
});

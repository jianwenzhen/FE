// 实例化Vue对象
new Vue({
    el: "#vue-app",
    data: {
        health: 100,
        ended: false
    },
    methods: {
        punch() {
            this.health -= 10

            if (this.health <= 0) {
                this.ended = true
                this.health = 0
            }
        },
        restart() {
            this.health = 100
            this.ended = false
        }
    },
    computed: {
        
    }
});
